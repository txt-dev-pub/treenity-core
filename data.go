package treenityCore

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/stretchr/signature"
	"io"
	"net/url"
	"strings"
)

var (
	// PathSeparator is the character used to separate the elements
	// of the keypath.
	//
	// For example, `location.address.city`
	PathSeparator string = "."

	// SignatureSeparator is the character that is used to
	// separate the Base64 string from the security signature.
	SignatureSeparator = "_"
)

type Data map[string]any

const NodeDataOnStart = "__start"
const NodeDataOnComplete = "__success"
const NodeDataOnError = "__error"

func NodeDataFromParams(params map[string]any) Data {
	res := Data{}

	for key, value := range params {
		res[key] = value
	}

	return res
}

func NodeDataError(err error) Data {
	return Data{}.Error(err)
}

func NodeDataErr(err string) Data {
	return Data{}.Error(errors.New(err))
}

func NodeDataComplete() Data {
	return Data{}.Complete()
}

func (nd Data) Complete() Data {
	nd[NodeDataOnComplete] = true

	return nd
}

func (nd Data) Error(err error) Data {
	nd[NodeDataOnError] = err.Error()
	return nd
}

func (nd Data) SetEvent(key string) Data {
	nd[key] = true
	return nd
}

func (nd Data) IsExist(key string) bool {
	_, ok := nd[key]

	return ok
}

func (nd Data) IsComplete() bool {
	_, ok := nd[NodeDataOnComplete]

	return ok
}

func (nd Data) IsError() bool {
	_, ok := nd[NodeDataOnError]

	return ok
}

func (nd Data) GetError() string {
	val, ok := nd[NodeDataOnError]
	if !ok {
		return ""
	}

	return val.(string)
}

func (np Data) GetBytes(key string) []byte {
	switch v := np[key].(type) {
	case []byte:
		return v
	case string:
		return []byte(v)
	default:
		return nil
	}
}

func (np Data) GetUint(key string) uint {
	switch np[key].(type) {
	case uint:
		return np[key].(uint)
	default:
		return 0
	}
}

func (np Data) GetMeta(key string) *Meta {
	switch v := np[key].(type) {
	case *Meta:
		return v
	default:
		var result Meta
		err := mapstructure.Decode(v, &result)
		if err != nil {
			return nil
		}

		return &result
	}
}

// TODO Test on data
// TODO Test on nil or empty object
func (np Data) GetNode(key string) *TreeNode {
	switch v := np[key].(type) {
	case *TreeNode:
		return v
	default:
		var result TreeNode
		err := mapstructure.Decode(v, &result)
		if err != nil || len(result.Id) == 0 {
			return nil
		}

		p := result.GetProps()
		if p != nil {
			for _, m := range p.Metas {
				m.Node = &result
			}
		}

		result.SetData(p)

		return &result
	}
}

func NodeDataValue[T any](np Data, key string) *T {
	if v, ok := np[key]; ok {
		switch val := v.(type) {
		case T:
			return &val
		default:
			return nil
		}
	}

	return nil
}

func (np Data) GetInt(key string) int {
	switch np[key].(type) {
	case int:
		return np[key].(int)
	default:
		return 0
	}
}

func (np Data) GetBool(key string) bool {
	switch np[key].(type) {
	case bool:
		return np[key].(bool)
	default:
		return false
	}
}

func (np Data) Set(key string, value any) Data {
	np[key] = value
	return np
}

func (np Data) Get(key string) any {
	return np[key]
}

func (np Data) GetString(key string) string {
	switch v := np[key].(type) {
	case string:
		return v
	case []byte:
		return string(v)
	default:
		return ""
	}
}

func (np Data) GetLink(key string) Link {
	switch v := np[key].(type) {
	case string:
		return Link(v)
	case []byte:
		return Link(v)
	case Link:
		return v
	default:
		return ""
	}
}

func (np Data) GetByte(key string) []byte {
	switch v := np[key].(type) {
	case []byte:
		return v
	case string:
		return []byte(v)
	default:
		return nil
	}
}

// ParamValue Getting value by key from Data by selected type
func ParamValue[T any](np Data, key string) *T {
	value, ok := np[key]
	if !ok {
		return nil
	}

	switch v := value.(type) {
	case T:
		return &v
	default:
		var result T
		mapstructure.Decode(v, &result)
		return &result
	}
}

// ParamValueP Getting value by path from Data by selected type
func ParamValueP[T any](np Data, path string) *T {
	if !np.Has(path) {
		return nil
	}

	switch v := np.GetP(path).(type) {
	case T:
		return &v
	default:
		var result T
		mapstructure.Decode(v, &result)
		return &result
	}
}

// NewParams To easily create Data:
//
//	m := objects.M("name", "Mat", "age", 29, "subobj", objects.M("active", true))
//
//	// creates a Data equivalent to
//	m := map[string]interface{}{"name": "Mat", "age": 29, "subobj": map[string]interface{}{"active": true}}
func NewParams(keyAndValuePairs ...interface{}) Data {
	newParams := make(Data)
	keyAndValuePairsLen := len(keyAndValuePairs)

	if keyAndValuePairsLen%2 != 0 {
		panic("NewParams must have an even number of arguments following the 'key, value' pattern.")
	}

	for i := 0; i < keyAndValuePairsLen; i = i + 2 {
		key := keyAndValuePairs[i]
		value := keyAndValuePairs[i+1]

		// make sure the key is a string
		keyString, keyStringOK := key.(string)
		if !keyStringOK {
			panic(fmt.Sprintf("NewParams must follow 'string, interface{}' pattern.  %s is not a valid key.", keyString))
		}

		newParams[keyString] = value
	}

	return newParams
}

// NewParamsFromJSON creates a new map from a JSON string representation
func NewParamsFromJSON(data string) (Data, error) {
	var unmarshalled map[string]interface{}

	err := json.Unmarshal([]byte(data), &unmarshalled)

	if err != nil {
		return nil, errors.New("Data: JSON decode failed with: " + err.Error())
	}

	return unmarshalled, nil
}

// NewParamsFromBase64String creates a new map from a Base64 string representation
func NewParamsFromBase64String(data string) (Data, error) {
	decoder := base64.NewDecoder(base64.StdEncoding, strings.NewReader(data))

	decoded, err := io.ReadAll(decoder)
	if err != nil {
		return nil, err
	}

	return NewParamsFromJSON(string(decoded))
}

// NewParamsFromSignedBase64String creates a new map from a signed Base64 string representation
func NewParamsFromSignedBase64String(data, key string) (Data, error) {
	parts := strings.Split(data, SignatureSeparator)
	if len(parts) != 2 {
		return nil, errors.New("Data: Signed base64 string is malformed.")
	}

	sig := signature.HashWithKey([]byte(parts[0]), []byte(key))
	if parts[1] != sig {
		return nil, errors.New("Data: Signature for Base64 data does not match.")
	}

	return NewParamsFromBase64String(parts[0])
}

// GetP gets the value from the map.  Supports deep nesting of other maps,
// For example:
//
//	m = Data{"name":Data{"First": "Mat", "Last": "Ryer"}}
//
//	m.GetP("name.Last")
//	// returns "Ryer"
func (d Data) GetP(keypath string) interface{} {
	segs := strings.Split(keypath, PathSeparator)

	obj := d

	for fieldIndex, field := range segs {

		if fieldIndex == len(segs)-1 {
			return obj[field]
		}

		switch obj[field].(type) {
		case Data:
			obj = obj[field].(Data)
		case map[string]interface{}:
			obj = obj[field].(map[string]interface{})
		}
	}

	return obj
}

// GetParams gets another Data from this one, or panics if the object is missing or not a Data.
func (d Data) GetParams(keypath string) Data {
	return d.GetP(keypath).(Data)
}

// GetStringP gets a string value from the map at the given keypath, or panics if one
// is not available, or is of the wrong type.
func (d Data) GetStringP(keypath string) string {
	return d.GetP(keypath).(string)
}

// GetOrDefault gets the value at the specified keypath, or returns the defaultValue if
// none could be found.
func (d Data) GetOrDefault(keypath string, defaultValue interface{}) interface{} {
	obj := d.GetP(keypath)
	if obj == nil {
		return defaultValue
	}

	return obj
}

// GetStringOrDefault gets the string value at the specified keypath,
// or returns the defaultValue if none could be found.  Will panic if the
// object is there but of the wrong type.
func (d Data) GetStringOrDefault(keypath, defaultValue string) string {
	obj := d.GetP(keypath)
	if obj == nil {
		return defaultValue
	}

	return obj.(string)
}

// GetStringOrEmpty gets the string value at the specified keypath or returns
// an empty string if none could be fo und. Will panic if the object is there
// but of the wrong type.
func (d Data) GetStringOrEmpty(keypath string) string {
	return d.GetStringOrDefault(keypath, "")
}

// SetP sets a value in the map.  Supports dot syntax to set deep values.
//
// For example,
//
//	m.Set("name.first", "Mat")
//
// The above code sets the 'first' field on the 'name' object in the m Data.
//
// If objects are nil along the way, Set creates new Data objects as needed.
func (d Data) SetP(keypath string, value interface{}) Data {
	var segs []string
	segs = strings.Split(keypath, PathSeparator)

	obj := d

	for fieldIndex, field := range segs {

		if fieldIndex == len(segs)-1 {
			obj[field] = value
		}

		if _, exists := obj[field]; !exists {
			obj[field] = make(Data)
			obj = obj[field].(Data)
		} else {
			switch obj[field].(type) {
			case Data:
				obj = obj[field].(Data)
			case map[string]interface{}:
				obj = obj[field].(map[string]interface{})
			}
		}

	}

	return d
}

// Exclude returns a new Data with the keys in the specified []string
// excluded.
func (d Data) Exclude(exclude []string) Data {
	excluded := make(Data)
	for k, v := range d {
		var shouldInclude bool = true
		for _, toExclude := range exclude {
			if k == toExclude {
				shouldInclude = false
				break
			}
		}
		if shouldInclude {
			excluded[k] = v
		}
	}

	return excluded
}

// Copy creates a shallow copy of the Data.
func (d Data) Copy() Data {
	copied := make(Data)
	for k, v := range d {
		copied[k] = v
	}

	return copied
}

// Merge blends the specified map with a copy of this map and returns the result.
//
// Keys that appear in both will be selected from the specified map.
func (d Data) Merge(merge Data) Data {
	return d.Copy().MergeHere(merge)
}

// MergeHere blends the specified map with this map and returns the current map.
//
// Keys that appear in both will be selected from the specified map.  The original map
// will be modified.
func (d Data) MergeHere(merge Data) Data {
	for k, v := range merge {
		d[k] = v
	}

	return d
}

// Has gets whether the Data has the specified field or not. Supports deep nesting of other maps.
//
// For example:
//
//	m := map[string]interface{}{"parent": map[string]interface{}{"childname": "Luke"}}
//	m.Has("parent.childname")
//	// return true
func (d Data) Has(path string) bool {
	return d.GetP(path) != nil
}

// MSI is a shortcut method to get the current map as a
// normal map[string]interface{}.
func (d Data) MSI() map[string]interface{} {
	return d
}

// JSON converts the map to a JSON string
func (d Data) JSON() (string, error) {
	result, err := json.Marshal(d)

	if err != nil {
		err = errors.New("Data: JSON encode failed with: " + err.Error())
	}

	return string(result), err
}

// Base64 converts the map to a base64 string
func (d Data) Base64() (string, error) {
	var buf bytes.Buffer

	jsonData, err := d.JSON()
	if err != nil {
		return "", err
	}

	encoder := base64.NewEncoder(base64.StdEncoding, &buf)
	encoder.Write([]byte(jsonData))
	encoder.Close()

	return buf.String(), nil
}

// SignedBase64 converts the map to a base64 string and signs it using the
// provided key. The returned data is the base64 string plus an appended signature.
//
// Will return an error if Base64ing the map fails.
func (d Data) SignedBase64(key string) (string, error) {
	b64, err := d.Base64()
	if err != nil {
		return "", err
	}

	sig := signature.HashWithKey([]byte(b64), []byte(key))

	return b64 + SignatureSeparator + sig, nil

}

// Hash gets the hash of the map with no security key.
//
// Will return an error if Base64ing the map fails.
func (d Data) Hash() (string, error) {
	return d.HashWithKey("")
}

// HashWithKey gets a hash of the map, signed by the
// specified security key.
//
// Will return an error if Base64ing the map fails.
func (d Data) HashWithKey(key string) (string, error) {
	b64, err := d.Base64()
	if err != nil {
		return "", err
	}

	sig := signature.HashWithKey([]byte(b64), []byte(key))

	return sig, nil
}

/*
	URL Query
	------------------------------------------------
*/

// NewParamsFromURLQuery generates a new map by parsing the specified
// query.
//
// For queries with multiple values, the first value is selected.
func NewParamsFromURLQuery(query string) (Data, error) {
	vals, err := url.ParseQuery(query)

	if err != nil {
		return nil, err
	}

	return NewParamsFromURLValues(vals)
}

func (d Data) URLValues() url.Values {
	vals := make(url.Values)

	for k, v := range d {
		vals.Set(k, fmt.Sprintf("%v", v))
	}

	return vals
}

func NewParamsFromURLValues(vals url.Values) (Data, error) {
	m := make(Data)
	for k, vals := range vals {
		m.SetP(k, vals[0])
	}

	return m, nil
}

// URLQuery gets an encoded URL query representing the given
// map.
func (d Data) URLQuery() (string, error) {
	return d.URLValues().Encode(), nil
}

// Transform builds a new map giving the transformer a chance
// to change the keys and values as it goes.
func (d Data) Transform(transformer func(key string, value interface{}) (string, interface{})) Data {
	m := make(Data)
	for k, v := range d {
		modifiedKey, modifiedVal := transformer(k, v)
		m[modifiedKey] = modifiedVal
	}
	return m
}

// TransformKeys builds a new map using the specified key mapping.
//
// Unspecified keys will be unaltered.
func (d Data) TransformKeys(mapping map[string]string) Data {
	return d.Transform(func(key string, value interface{}) (string, interface{}) {
		if newKey, ok := mapping[key]; ok {
			return newKey, value
		}

		return key, value
	})
}

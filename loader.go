package treenityCore

type Loader interface {
	Get(data Link) *TreeNode
	Children(data Link) []*TreeNode
}

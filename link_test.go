package treenityCore

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLinkContext(t *testing.T) {
	context1 := Link("tree:/abcbb/$_meta_id_").Context()
	assert.Equal(t, context1, "tree")

	context2 := Link("tree+svc:/abcbb/$_meta_id_").Context()
	assert.Equal(t, context2, "tree+svc")

	context3 := Link("tree-svc:/abcbb/$_meta_id_").Context()
	assert.Equal(t, context3, "tree-svc")

	context4 := Link("tree_svc:/abcbb/$_meta_id_").Context()
	assert.Equal(t, context4, "tree_svc")

	context5 := Link("tree:svc:/abcbb/$_meta_id_").Context()
	assert.Equal(t, context5, "tree")
}

func TestLinkField(t *testing.T) {
	field1 := Link("tree:/abcbb/$_meta_id_#field").Field()
	assert.Equal(t, "field", field1)

	field2 := Link("tree:/abcbb/$_meta_id_#field?q=2").Field()
	assert.Equal(t, "field", field2)
}

func TestLinkMeta(t *testing.T) {
	meta := Link("tree:/abcbb/$_meta_id_#field").Meta()
	assert.Equal(t, "_meta_id_", meta)

	meta1 := Link("tree:/abcbb/$kk999").Meta()
	assert.Equal(t, "kk999", meta1)

	meta2 := Link("tree:/abcbb#fff").Meta()
	assert.Equal(t, "", meta2)

	meta3 := Link("tree:/abcbb$kk999#fff").Meta()
	assert.Equal(t, "", meta3)

	meta4 := Link("tree:/abcbb/$&&kk999#fff").Meta()
	assert.Equal(t, "", meta4)
}

func TestLinkFieldJoin(t *testing.T) {
	field1 := Link("tree:/abcbb/wof#field").Join("mew")
	assert.Equal(t, Link("tree:/abcbb/wof/mew#field"), field1)

	// TODO: Fix it
	field2 := Link("tree:/abcbb/wof/$meta_id#field").Join("mew")
	assert.Equal(t, Link("tree:/abcbb/wof/mew#field"), field2)
}

func TestProtocolAndHost(t *testing.T) {
	baseUrl := Link("txt://private.repo/some/node/meta").ContextAndHost()
	assert.Equal(t, "txt://private.repo", baseUrl)
}

func TestProtocolAndHost_NoHost(t *testing.T) {
	baseUrl := Link("txt:/some/node/meta").ContextAndHost()
	assert.Equal(t, "", baseUrl)
}

func TestLinkAddField(t *testing.T) {
	field1 := Link("tree:/abcbb/wof").AddField("field")
	assert.Equal(t, Link("tree:/abcbb/wof#field"), field1)

	field2 := Link("tree:/abcbb/wof#mew").AddField("field")
	assert.Equal(t, Link("tree:/abcbb/wof#field"), field2)
}

func TestLinkSetContext(t *testing.T) {
	field1 := Link("tree:/abcbb/wof").SetContext("mew")
	assert.Equal(t, Link("mew:/abcbb/wof"), field1)

	field2 := Link("/abcbb/wof").SetContext("mew")
	assert.Equal(t, Link("mew:/abcbb/wof"), field2)
}

func TestCreateLink(t *testing.T) {
	lnk1 := CreateStructLinkN("/abcbb")
	lnk2 := CreateStructLinkNM("/abcbb", "_meta_id_")
	lnk3 := CreateStructLinkNMF("/abcbb", "_meta_id_", "_field_")
	lnk4 := CreateStructLinkNF("/abcbb", "_field_")

	assert.Equal(t, Link("tree:/abcbb"), lnk1)
	assert.Equal(t, Link("tree:/abcbb/$_meta_id_"), lnk2)
	assert.Equal(t, Link("tree:/abcbb/$_meta_id_#_field_"), lnk3)
	assert.Equal(t, Link("tree:/abcbb#_field_"), lnk4)
}

func TestParseLink(t *testing.T) {
	lnk1 := Link("tree:/abcbb")
	lnk2 := Link("tree:/abcbb/$_meta_id_")
	lnk3 := Link("tree:/abcbb/$_meta_id_#_field_")
	lnk4 := Link("tree:/abcbb#_field_")

	data1, err := lnk1.Parse()
	assert.NoError(t, err)
	assert.Equal(t, data1.Schema, "tree")
	assert.Equal(t, data1.path[0], "abcbb")
	assert.Equal(t, len(data1.path), 1)

	data2, err := lnk2.Parse()
	assert.NoError(t, err)
	assert.Equal(t, data2.Schema, "tree")
	assert.Equal(t, data2.path[0], "abcbb")
	assert.Equal(t, data2.path[1], "$_meta_id_")

	data3, err := lnk3.Parse()
	assert.NoError(t, err)
	assert.Equal(t, data3.Schema, "tree")
	assert.Equal(t, data3.path[0], "abcbb")
	assert.Equal(t, data3.path[1], "$_meta_id_")
	assert.Equal(t, data3.hash, "_field_")

	data4, err := lnk4.Parse()
	assert.NoError(t, err)
	assert.Equal(t, data4.Schema, "tree")
	assert.Equal(t, data4.path[0], "abcbb")
	assert.Equal(t, len(data4.path), 1)
	assert.Equal(t, data4.hash, "_field_")
}

func TestLinkSetHost(t *testing.T) {
	base := "txt://tree.repo"

	l1 := Link("txt://core.repo/node/$meta#field")
	assert.Equal(t, Link("txt://tree.repo/node/$meta#field"), l1.SetHost(base))

	l2 := Link("/node/$meta#field")
	assert.Equal(t, Link("txt://tree.repo/node/$meta#field"), l2.SetHost(base))

	l3 := Link("txt:/node/$meta#field")
	assert.Equal(t, Link("txt://tree.repo/node/$meta#field"), l3.SetHost(base))
}

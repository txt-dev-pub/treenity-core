package treenityCore

import (
	"fmt"
	"strings"
)

type NodeLinkData = LinkData

func (n *NodeLinkData) NodesPath(suffix string) string {
	val := fmt.Sprintf("/%s", strings.Join(n.Nodes(), "/"))
	if len(suffix) > 0 {
		if !strings.HasSuffix(val, suffix) {
			val = val + suffix
		}
	}

	return val
}

func (n *NodeLinkData) NodeMetaPath() string {
	if n.MetaExist() {
		return fmt.Sprintf("/%s/%s", strings.Join(n.Nodes(), "/"), *n.Meta())
	}

	return n.NodesPath("")
}

func (n *NodeLinkData) ParentLink(context string) Link {
	return CreateStructLinkNC(context, n.NodesPathWithTrim(1))
}

func (n *NodeLinkData) NodesPathWithTrim(v int) string {
	return fmt.Sprintf("/%s", strings.Join(n.NodesWithTrim(v), "/"))
}

func (n *NodeLinkData) IsSubPath(lnk Link) bool {
	_ld, err := lnk.Parse()
	if err != nil {
		return false
	}

	for i, p := range _ld.path {
		if strings.HasPrefix(p, "$") {
			break
		}

		if len(n.path) <= i || p != n.path[i] {
			return false
		}
	}

	return true
}

func (n *NodeLinkData) NodesExist() bool {
	return len(n.path) > 0
}

func (n *NodeLinkData) Nodes() []string {
	return n.NodesWithTrim(0)
}

func (n *NodeLinkData) LastNode() string {
	var index int
	if strings.HasPrefix(n.path[len(n.path)-1], "$") {
		index = len(n.path) - 2
	} else {
		index = len(n.path) - 1
	}

	if index < 0 {
		return ""
	}

	return n.path[index]
}

func (n *NodeLinkData) NodesWithTrim(v int) []string {
	var bound int
	l := len(n.path)
	if l < 1 {
		return []string{}
	}

	if strings.HasPrefix(n.path[l-1], "$") {
		bound = len(n.path) - (v + 1)
	} else {
		bound = len(n.path) - v
	}

	if bound < 1 {
		return []string{}
	}

	return n.path[:bound]
}

func (n *NodeLinkData) Meta() *string {
	item := n.path[len(n.path)-1]
	if strings.HasPrefix(item, "$") {
		val := item[1:]
		return &val
	}

	return nil
}

func (n *NodeLinkData) MetaExist() bool {
	l := len(n.path)
	if l < 1 {
		return false
	}

	item := n.path[l-1]
	return strings.HasPrefix(item, "$")
}

func (n *NodeLinkData) Field() *string {
	if len(n.hash) > 0 {
		return &n.hash
	}

	return nil
}

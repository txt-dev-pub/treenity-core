package treenityCore

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestMetaMarshal(t *testing.T) {
	m := &Meta{
		Id:   "my-id",
		Type: "node-tree",
		Data: Data{
			"field1": "value1",
			"field2": "value2",
			"field3": map[string]any{
				"val": 900,
			},
		},
	}

	data, err := json.Marshal(m)

	strData := string(data)
	assert.True(t, strings.Contains(strData, "\"$id\":\"my-id\""))
	assert.True(t, strings.Contains(strData, "\"$type\":\"node-tree\""))
	assert.True(t, strings.Contains(strData, "\"$name\":\"\""))
	assert.True(t, strings.Contains(strData, "\"field1\":\"value1\""))
	assert.True(t, strings.Contains(strData, "\"field2\":\"value2\""))
	assert.True(t, strings.Contains(strData, "\"field3\":{\"val\":900}"))
	assert.NoError(t, err)
}

func TestMetaMarshal_EmptyData(t *testing.T) {
	m := &Meta{
		Id:   "my-id",
		Type: "node-tree",
		Data: Data{},
	}

	data, err := json.Marshal(m)

	strData := string(data)
	assert.NoError(t, err)
	assert.True(t, strings.Contains(strData, "\"$id\":\"my-id\""))
	assert.True(t, strings.Contains(strData, "\"$type\":\"node-tree\""))
	assert.True(t, strings.Contains(strData, "\"$name\":\"\""))
}

func TestMetaUnMarshal(t *testing.T) {
	text := "{\"$id\":\"my-id\",\"$name\":\"hey\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	var m Meta
	err := json.Unmarshal([]byte(text), &m)
	assert.NoError(t, err)
	//assert.True(t, len(m.Data.GetString("$id")) == 0)
	//assert.True(t, len(m.Data.GetString("$type")) == 0)
	//assert.True(t, len(m.Data.GetString("$name")) == 0)

	for k, _ := range m.Data {
		assert.False(t, strings.HasPrefix(k, "$"))
	}
}

func TestMetaUnMarshal_NilObject(t *testing.T) {
	text := "{}"
	var m *Meta
	err := json.Unmarshal([]byte(text), &m)
	assert.Error(t, err)
	assert.NotNil(t, m)
}

// Check unmarshal on no ID
func TestMetaUnMarshal_NoId(t *testing.T) {
	text := "{\"$name\":\"mew\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	var m Meta
	err := json.Unmarshal([]byte(text), &m)

	assert.NoError(t, err)
}

// Check unmarshal on no ID
func TestMetaUnMarshal_IdIsNum(t *testing.T) {
	text := "{\"$id\":1,\"$name\":\"\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	var m Meta
	err := json.Unmarshal([]byte(text), &m)

	assert.Error(t, err)
	assert.Equal(t, "field.$id.not-string", err.Error())
}

// Check unmarshal on no Name
func TestMetaUnMarshal_NoName(t *testing.T) {
	text := "{\"$id\":\"some-id\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	var m Meta
	err := json.Unmarshal([]byte(text), &m)

	assert.Error(t, err)
	assert.Equal(t, "field.$name.not-found", err.Error())
}

// Check unmarshal on no Name
func TestMetaUnMarshal_NameIsEmpty(t *testing.T) {
	text := "{\"$id\":\"some-id\",\"$name\":\"\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	var m Meta
	err := json.Unmarshal([]byte(text), &m)

	assert.Error(t, err)
	assert.Equal(t, "field.$name.not-string-or-empty", err.Error())
}

// Check unmarshal on no Name
func TestMetaUnMarshal_NameIsNotString(t *testing.T) {
	text := "{\"$id\":\"some-id\",\"$name\":2,\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	var m Meta
	err := json.Unmarshal([]byte(text), &m)

	assert.Error(t, err)
	assert.Equal(t, "field.$name.not-string-or-empty", err.Error())
}

// BenchmarkMetaJsonUnmarshal-10    	  578282	      2017 ns/op
func BenchmarkMetaJsonUnmarshal(b *testing.B) {
	text := "{\"$id\":\"my-id\",\"$name\":\"hey\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	for i := 0; i < b.N; i++ {
		var m Meta
		json.Unmarshal([]byte(text), &m)
	}
}

// BenchmarkMetaJsonMarshal-10    	  401788	      2706 ns/op
func BenchmarkMetaJsonMarshal(b *testing.B) {
	m := &Meta{
		Id:   "my-id",
		Type: "node-tree",
		Tags: []string{
			"autostart",
		},
		Data: Data{
			"field1": "value1",
			"field2": "value2",
			"field3": map[string]any{
				"val": 900,
			},
		},
	}
	for i := 0; i < b.N; i++ {
		json.Marshal(m)
	}
}

type MetaBuildIn struct {
	Id   string   `json:"$id" mapstructure:"$id"`
	Name string   `json:"$name" mapstructure:"$name"`
	Tags []string `json:"$tg,omitempty" mapstructure:"$tg,omitempty"`
	Type string   `json:"$type,omitempty" mapstructure:"$type,omitempty"`
	Node any      `json:"-"`
	Data Data     `json:"data,omitempty" mapstructure:",remain"`
}

// BenchmarkMetaBuildInJsonUnmarshal-10    	  871354	      1376 ns/op
func BenchmarkMetaBuildInJsonUnmarshal(b *testing.B) {
	text := "{\"$id\":\"my-id\",\"$name\":\"hey\",\"$type\":\"node-tree\",\"field1\":\"value1\",\"field2\":\"value2\",\"field3\":{\"val\":900}}"
	for i := 0; i < b.N; i++ {
		var m MetaBuildIn
		json.Unmarshal([]byte(text), &m)
	}
}

// BenchmarkMetaJsonBuildInMarshal-10    	  925071	      1247 ns/op
func BenchmarkMetaJsonBuildInMarshal(b *testing.B) {
	m := &MetaBuildIn{
		Id:   "my-id",
		Type: "node-tree",
		Tags: []string{
			"autostart",
		},
		Data: Data{
			"field1": "value1",
			"field2": "value2",
			"field3": map[string]any{
				"val": 900,
			},
		},
	}
	for i := 0; i < b.N; i++ {
		json.Marshal(m)
	}
}

func TestMetaGetData(t *testing.T) {
	m := &Meta{
		Id:   "my-id",
		Name: "general",
		Type: "node-tree",
		Data: Data{
			"title": "FolderName",
			"links": []any{
				map[string]any{
					"p": "some-p",
					"v": "some-v",
				},
			},
			"path":        "/str",
			"defaultMeta": "example",
			"metas": map[string]*Meta{
				"child-id": {
					Id:   "child-id",
					Name: "Child",
					Type: "child-type",
				},
			},
		},
	}

	data, err := json.Marshal(m)
	assert.NoError(t, err)

	var parsedMeta Meta
	err = json.Unmarshal(data, &parsedMeta)
	assert.NoError(t, err)

	treeProps := GetData[TreeProps](&parsedMeta)
	assert.NotNil(t, treeProps)
	assert.Equal(t, "some-p", treeProps.Links[0].Path)
	assert.Equal(t, "some-v", treeProps.Links[0].Value)
	assert.Equal(t, "/str", treeProps.Path)
	assert.Equal(t, "FolderName", treeProps.Title)

	nodeProps := GetData[NodeProps](&parsedMeta)
	assert.NotNil(t, nodeProps)
	assert.Equal(t, "example", nodeProps.DefaultMeta)
}

package treenityCore

import (
	"errors"
	"fmt"
	"sync"
)

type Registry interface {
	Add(Context string, Type string, instance Runnable)
	Remove(Context string, Type string)
	AddService(Type string, service Runnable)
	Get(p RegParams) (Runnable, error)
}

type registry struct {
	sync.Mutex
	items map[string]Runnable
}

//func (h *registry) List() map[string]interface{} {
//	return h.items
//}

type RegParams struct {
	Context string `json:"context"`
	Type    string `json:"type"`
}

func (p *RegParams) getKey() string {
	return fmt.Sprintf("%s_%s", p.Type, p.Context)
}

func (h *registry) Add(Context string, Type string, instance Runnable) {
	h.Lock()
	p := RegParams{
		Context: Context,
		Type:    Type,
	}
	h.items[p.getKey()] = instance
	h.Unlock()
}

func (h *registry) Remove(Context string, Type string) {
	h.Lock()
	p := RegParams{
		Context: Context,
		Type:    Type,
	}
	delete(h.items, p.getKey())
	h.Unlock()
}

func (h *registry) AddService(Type string, service Runnable) {
	h.Add(ServiceContext, Type, service)
}

func (h *registry) Get(p RegParams) (Runnable, error) {
	h.Lock()
	defer h.Unlock()
	if val, ok := h.items[p.getKey()]; ok {
		return val, nil
	}

	return nil, errors.New("registry.item.not-found")
}

func NewRegistry() Registry {
	return &registry{
		items: make(map[string]Runnable),
	}
}

module gitlab.com/txt-dev-pub/treenity-core

go 1.19

require (
	github.com/mitchellh/mapstructure v1.5.0
	github.com/stretchr/testify v1.8.1
	github.com/tidwall/gjson v1.14.4
	github.com/tidwall/sjson v1.2.5
	golang.org/x/exp v0.0.0-20221126150942-6ab00d035af9
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/evanphx/json-patch/v5 v5.6.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/lithammer/shortuuid/v4 v4.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/afero v1.9.3 // indirect
	github.com/stretchr/signature v0.0.0-20160104132143-168b2a1e1b56 // indirect
	github.com/stretchr/stew v0.0.0-20130812190256-80ef0842b48b // indirect
	github.com/stretchr/tracer v0.0.0-20140124184152-66d3696bba97 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	gitlab.com/nikita.morozov/ms-shared v1.11.0 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	gorm.io/gorm v1.22.2 // indirect
)

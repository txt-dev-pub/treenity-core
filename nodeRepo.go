package treenityCore

import (
	"context"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type NodeRepo interface {
	Get(link Link) *TreeNode
	LinkTo(from Link, to Link) error
	List(ctx context.Context, link Link, opts *sharedModels.ListOptions) ([]*TreeNode, error)
	Copy(from Link, to Link) error
	Patch(link Link, patches []byte) error
	HasChildren(link Link) (bool, error)

	Create(ctx context.Context, node *TreeNode) (*TreeNode, error)
	CreateChild(ctx context.Context, parentId *string, n *TreeNode) (*TreeNode, error)
	FindById(ctx context.Context, id string) (*TreeNode, error)
	FindBySlug(ctx context.Context, slug string) (*TreeNode, error)
	FindByIdOrSlug(ctx context.Context, v string, pid any) (*TreeNode, error)
	//FindOne(ctx context.Context, query bson.M) (*TreeNode, error)
	//Find(ctx context.Context, query bson.M, opts *sharedModels.ListOptions) (*[]TreeNode, error)
	FindMeta(ctx context.Context, data map[string]any) ([]*Meta, error)
	FindMetaTT(ctx context.Context, tag string, typeValue string) ([]*Meta, error)
	//Delete(ctx context.Context, query bson.M) (int64, error)
	//DeleteMany(ctx context.Context, query bson.M) (int64, error)
}

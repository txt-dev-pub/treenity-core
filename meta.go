package treenityCore

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/lithammer/shortuuid/v4"
	"github.com/mitchellh/mapstructure"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"path"
)

type Meta struct {
	Id   string   `json:"$id" mapstructure:"$id"`
	Name string   `json:"$name" mapstructure:"$name"`
	Tags []string `json:"$tg,omitempty" mapstructure:"$tg,omitempty"`
	Type string   `json:"$type,omitempty" mapstructure:"$type,omitempty"`
	Node any      `json:"-"`
	Data `json:"data,omitempty" mapstructure:",remain"`
}

func (s *Meta) MarshalJSON() ([]byte, error) {
	if len(s.Id) == 0 {
		s.Id = shortuuid.New()
	}

	var err error
	var res string
	var data []byte
	if s.Data != nil {
		data, err = json.Marshal(s.Data)
	}

	if len(data) > 2 {
		res = fmt.Sprintf("{\"$id\":\"%s\",\"$name\":\"%s\",\"$type\":\"%s\",%s}", s.Id, s.Name, s.Type, string(data[1:len(data)-1]))
	} else {
		res = fmt.Sprintf("{\"$id\":\"%s\",\"$name\":\"%s\",\"$type\":\"%s\"}", s.Id, s.Name, s.Type)
	}

	if s.Tags != nil && len(s.Tags) > 0 {
		res, err = sjson.Set(res, "$tg", s.Tags)
	}

	return []byte(res), err
}

func (s *Meta) UnmarshalJSON(data []byte) error {
	m, ok := gjson.ParseBytes(data).Value().(map[string]any)
	if !ok {
		return errors.New("meta.parse-failed")
	}

	i, ok := m["$id"]
	if ok {
		(*s).Id, ok = i.(string)
		if !ok {
			return errors.New("field.$id.not-string")
		}
	}

	i, ok = m["$name"]
	if !ok {
		return errors.New("field.$name.not-found")
	}

	(*s).Name, ok = i.(string)
	if !ok || len((*s).Name) == 0 {
		return errors.New("field.$name.not-string-or-empty")
	}

	switch v := m["$type"].(type) {
	case string:
		(*s).Type = v
	}

	if len((*s).Id) == 0 {
		(*s).Id = shortuuid.New()
	}

	// TODO optimize it
	tg := gjson.GetBytes(data, "$tg")
	if tg.IsArray() {
		for _, i := range tg.Array() {
			s.Tags = append(s.Tags, i.String())
		}

		delete(m, "$tg")
	}
	//for k, _ := range m {
	//	if strings.HasPrefix(k, "$") {
	//		delete(m, k)
	//	}
	//}
	delete(m, "$id")
	delete(m, "$type")
	delete(m, "$name")

	s.Data = m

	return nil
}

func GetData[T any](m *Meta) *T {
	if m.Data == nil {
		return nil
	}

	var result T
	err := mapstructure.Decode(m.Data, &result)
	if err != nil {
		return nil
	}

	return &result
}

func (s *Meta) SetData(data any) *Meta {
	if s.Data == nil {
		s.Data = StructToMap(data)
	} else {
		switch v := data.(type) {
		case Data:
			s.Data.MergeHere(v)
		case map[string]any:
			s.Data.MergeHere(v)
		default:
			s.Data.MergeHere(StructToMap(v))
		}

	}

	return s
}

func (s *Meta) SetId(id string) {
	s.Id = id
}

func (s *Meta) SetName(name string) {
	s.Name = name
}

func (s *Meta) GetMeta() *Meta {
	return s
}

func (s *Meta) SetNode(n *Meta) {
	s.Node = n
}

func (s *Meta) Link(field *string) Link {
	return CreateLink(s, field)
}

func (s *Meta) RelativeLink(to string) Link {
	return Link(path.Join(string(CreateStructLinkN(s.GetPath())), to))
}

func (s *Meta) LinkWithContext(context string, field *string) Link {
	return CreateLinkWithContext(context, s, field)
}

func CreateLink(m *Meta, field *string) Link {
	if field == nil && m.Node != nil {
		return CreateStructLinkNM(m.GetPath(), m.Name)
	}

	if m.Node == nil {
		if field != nil {
			return CreateStructLinkNF(m.GetPath(), *field)
		}

		return CreateStructLinkN(m.GetPath())
	}

	return CreateLnk(TreeSchema, m.Node.(*Node).GetPath(), m.Name, field)
}

func CreateLinkWithContext(context string, m *Meta, field *string) Link {
	if field == nil {
		if m.Node == nil {
			return CreateStructLinkNC(context, m.GetPath())
		}

		return CreateStructLinkNMC(context, m.GetPath(), m.Name)
	}

	if m.Node == nil {
		return CreateStructLinkNFC(context, m.GetPath(), *field)
	}

	return CreateLnk(context, m.Node.(*Node).GetPath(), m.Name, field)
}

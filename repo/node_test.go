package repo

import (
	"context"
	"encoding/json"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"strings"
	"testing"
)

type FsRepoNodeSuite struct {
	suite.Suite

	basePath     string
	fs           afero.Fs
	repo         core.NodeRepo
	metaProvider IMetaProvider
}

func (s *FsRepoNodeSuite) BeforeTest(_, _ string) {
	s.fs = afero.NewMemMapFs()
	s.basePath = "/root/db"
	s.repo = NewFsNodeRepository(s.basePath, s.fs)
	s.metaProvider = MetaProvider(s.repo)

	s.fs.MkdirAll(s.basePath, 0755)
}

// TODO Test with path
// TODO Write|Read|Compare
// TODO Test_Create_Patch_AddMeta with $id
// TODO Test_Create_Patch_AddMeta with $name and $id

func TestAuthHandler(t *testing.T) {
	suite.Run(t, new(FsRepoNodeSuite))
}

func (s *FsRepoNodeSuite) createBaseStruct() {
	s.metaProvider.CreateOrGet(
		"tree:/",
		nil,
	)

	privateNode, _ := s.metaProvider.CreateOrGet(
		"tree:/private",
		nil,
	)
	servicesNode, _ := s.metaProvider.CreateOrGet(
		"tree:/private/services",
		privateNode,
	)

	s.metaProvider.CreateOrGet(
		"tree:/private/services/tree-node",
		servicesNode,
		&core.Meta{
			Name: "tree-service",
			Type: "tree-service",
			Data: core.Data{
				"execute_type": "node_tree",
			},
		},
		&core.Meta{
			Name: "rest-service",
			Type: "rest-service",
			Data: core.Data{
				"execute_type": "node_tree",
			},
		},
		&core.Meta{
			Name: "test-service",
			Type: "test-service",
			Data: core.Data{
				"field": "mew",
			},
		},
	)
}

func (s *FsRepoNodeSuite) Test_Create_WithMethod() {
	data := []byte(`{"$name":"test","$type":"node_tree"}`)
	var tn *core.TreeNode
	err := json.Unmarshal(data, &tn)
	tn.SetPath("/test")
	assert.NoError(s.T(), err)

	nTn, err := s.repo.Create(context.TODO(), tn)

	assert.Equal(s.T(), "test", nTn.Name)
	assert.NotNil(s.T(), nTn.GetProps())
	assert.Equal(s.T(), 0, len(nTn.GetProps().Metas))

	n2 := s.repo.Get("/test")
	assert.NotNil(s.T(), n2)
	assert.Equal(s.T(), "test", n2.Name)
	assert.NotNil(s.T(), n2.GetProps())
	assert.Equal(s.T(), 0, len(n2.GetProps().Metas))

	res, err := json.Marshal(n2)
	assert.NoError(s.T(), err)
	assert.Len(s.T(), res, 110)
	assert.True(s.T(), strings.Contains(string(res), `"metas":{}`))
	assert.True(s.T(), strings.Contains(string(res), `"defaultMeta":""`))
}

func (s *FsRepoNodeSuite) Test_Create_WithMethod_Patch() {
	data := []byte(`{"$name":"test","$type":"node_tree"}`)
	var tn *core.TreeNode
	err := json.Unmarshal(data, &tn)
	tn.SetPath("/test")
	assert.NoError(s.T(), err)

	nTn, err := s.repo.Create(context.TODO(), tn)

	assert.Equal(s.T(), "test", nTn.Name)
	assert.NotNil(s.T(), nTn.GetProps())
	assert.Equal(s.T(), 0, len(nTn.GetProps().Metas))

	n2 := s.repo.Get("/test")
	assert.NotNil(s.T(), n2)
	assert.Equal(s.T(), "test", n2.Name)
	assert.NotNil(s.T(), n2.GetProps())
	assert.Equal(s.T(), 0, len(n2.GetProps().Metas))

	res, err := json.Marshal(n2)
	assert.NoError(s.T(), err)
	assert.Len(s.T(), res, 110)
	assert.True(s.T(), strings.Contains(string(res), `"metas":{}`))
	assert.True(s.T(), strings.Contains(string(res), `"defaultMeta":""`))

	patchJSON := []byte(`[
		{"op": "add", "path": "/metas/rnd", "value": {"$type":"rnd-type","field1":"value1","field2":"value2","field3":{"val":900}}}
	]`)

	err = s.repo.Patch("tree:/test", patchJSON)
	assert.NoError(s.T(), err)

	ptn := s.repo.Get("tree:/test")
	assert.NotNil(s.T(), ptn)

	assert.NotNil(s.T(), ptn.GetProps())
	assert.Equal(s.T(), 1, len(ptn.GetProps().Metas))
	assert.Equal(s.T(), "rnd", ptn.GetProps().Metas["rnd"].Name)
	assert.Equal(s.T(), "rnd-type", ptn.GetProps().Metas["rnd"].Type)
	assert.Equal(s.T(), "value1", ptn.GetProps().Metas["rnd"].GetString("field1"))
}

func (s *FsRepoNodeSuite) Test_Create_WithMethod_WithMetas() {
	data := []byte(`{"$name":"test","$type":"node_tree","path":"/abc","metas":{"meta-name": {"$name":"meta-name","$type":"some-meta-type"}}}`)
	var tn *core.TreeNode
	err := json.Unmarshal(data, &tn)
	assert.NoError(s.T(), err)
	assert.Equal(s.T(), "test", tn.Name)

	nTn, err := s.repo.Create(context.TODO(), tn)
	assert.NoError(s.T(), err)
	assert.Equal(s.T(), "test", nTn.Name)

	props := tn.GetProps()
	assert.NotNil(s.T(), props)
	assert.Equal(s.T(), "meta-name", props.Metas["meta-name"].Name)
}

func (s *FsRepoNodeSuite) Test_Create_NodeWithMeta() {
	s.createBaseStruct()

	existTreeNode, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/$.json")
	assert.NoError(s.T(), err)
	assert.True(s.T(), existTreeNode)

	existTreeMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/tree-service.json")
	assert.NoError(s.T(), err)
	assert.True(s.T(), existTreeMeta)

	restTreeMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/rest-service.json")
	assert.NoError(s.T(), err)
	assert.True(s.T(), restTreeMeta)
}

func (s *FsRepoNodeSuite) Test_Create_PatchMeta() {
	patchJSON := []byte(`[
		{"op": "replace", "path": "/metas/tree-service/$type", "value": "tree-mod"},
		{"op": "add", "path": "/field", "value": "Figo"}
	]`)

	s.createBaseStruct()
	err := s.repo.Patch("tree:/private/services/tree-node", patchJSON)
	assert.NoError(s.T(), err)

	tn := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tn)
	assert.Equal(s.T(), "Figo", tn.Data.GetString("field"))
	assert.Equal(s.T(), "tree-mod", tn.GetProps().Metas["tree-service"].Type)
}

func (s *FsRepoNodeSuite) Test_GetEmpty() {
	s.createBaseStruct()
	tn := s.repo.Get("tree:/private")
	assert.NotNil(s.T(), tn)

	_, err := json.Marshal(tn)
	assert.NoError(s.T(), err)
	//assert.Equal(s.T(), "", res) // Here is error, it is ok, created for test
}

func (s *FsRepoNodeSuite) Test_GetWithMeta() {
	s.createBaseStruct()
	tn := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tn)
}

func (s *FsRepoNodeSuite) Test_Create_Patch_AddMeta() {
	patchJSON := []byte(`[
		{"op": "add", "path": "/metas/rnd", "value": {"$type":"rnd-type","field1":"value1","field2":"value2","field3":{"val":900}}}
	]`)

	s.createBaseStruct()
	err := s.repo.Patch("tree:/private/services/tree-node", patchJSON)
	assert.NoError(s.T(), err)

	rndMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/rnd.json")
	assert.NoError(s.T(), err)
	assert.True(s.T(), rndMeta)

	tn := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tn)
	assert.Equal(s.T(), "rnd", tn.GetProps().Metas["rnd"].Name)
	assert.Equal(s.T(), "value1", tn.GetProps().Metas["rnd"].Data.GetString("field1"))
}

func (s *FsRepoNodeSuite) Test_Create_Patch_RemoveMeta() {
	patchJSON := []byte(`[
		{"op": "remove", "path": "/metas/tree-service"}
	]`)

	s.createBaseStruct()
	err := s.repo.Patch("tree:/private/services/tree-node", patchJSON)
	assert.NoError(s.T(), err)

	treeMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/tree-service.json")
	assert.NoError(s.T(), err)
	assert.False(s.T(), treeMeta)

	tn := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tn)

	props := tn.GetProps()
	assert.Equal(s.T(), 2, len(props.Metas))
}

func (s *FsRepoNodeSuite) Test_Create_Patch_RemoveTwoMetaInOnePatch() {
	patchJSON := []byte(`[
		{"op": "remove", "path": "/metas/tree-service"},
		{"op": "remove", "path": "/metas/test-service"}
	]`)

	s.createBaseStruct()
	err := s.repo.Patch("tree:/private/services/tree-node", patchJSON)
	assert.NoError(s.T(), err)

	treeMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/tree-service.json")
	assert.NoError(s.T(), err)
	assert.False(s.T(), treeMeta)

	testMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/test-service.json")
	assert.NoError(s.T(), err)
	assert.False(s.T(), testMeta)

	tn := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tn)

	props := tn.GetProps()
	assert.Equal(s.T(), 1, len(props.Metas))
}

// TODO Bug in afero in mem on rename
//
//func (s *FsRepoNodeSuite) Test_Create_Patch_RenameNode() {
//	patchJSON := []byte(`[
//		{"op": "replace", "path": "/$name", "value": "more-than-node"},
//		{"op": "add", "path": "/field", "value":"mew"}
//	]`)
//
//	s.createBaseStruct()
//	err := s.repo.Patch("tree:/private/services/tree-node", patchJSON)
//	assert.NoError(s.T(), err)
//
//	treeMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/tree-node/tree-service.json")
//	assert.NoError(s.T(), err)
//	assert.True(s.T(), treeMeta)
//
//	testMeta, err := afero.Exists(s.fs, s.basePath+"/private/services/more-than-node/test-service.json")
//	assert.NoError(s.T(), err)
//	assert.True(s.T(), testMeta)
//
//	tn := s.repo.Get("tree:/private/services/more-than-node")
//	assert.NotNil(s.T(), tn)
//
//	props := tn.GetProps()
//	assert.Equal(s.T(), 3, len(props.Metas))
//}

func (s *FsRepoNodeSuite) Test_Create_Replace_DefaultMeta() {
	patchJSON := []byte(`[
		{"op":"add","path":"/defaultMeta","value":"tree-service"}
	]`)

	s.createBaseStruct()
	err := s.repo.Patch("tree:/private/services/tree-node", patchJSON)
	assert.NoError(s.T(), err)

	tn := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tn)
	assert.Equal(s.T(), "tree-service", tn.GetProps().DefaultMeta)

	patchReplaceJSON := []byte(`[
		{"op":"replace","path":"/defaultMeta","value":"rest-service"}
	]`)

	err = s.repo.Patch("tree:/private/services/tree-node", patchReplaceJSON)
	assert.NoError(s.T(), err)

	tnReplace := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tnReplace)
	assert.Equal(s.T(), "rest-service", tnReplace.GetProps().DefaultMeta)

	unknownReplaceJSON := []byte(`[
		{"op":"replace","path":"/defaultMeta","value":"unknown-service"},
		{"op": "add", "path": "/field", "value": "Figo"}
		{"op": "remove", "path": "/metas/tree-service"},
		{"op": "add", "path": "/field3", "value": "Figo2"}
	]`)

	err = s.repo.Patch("tree:/private/services/tree-node", unknownReplaceJSON)
	assert.NoError(s.T(), err)

	tnUnknown := s.repo.Get("tree:/private/services/tree-node")
	assert.NotNil(s.T(), tnUnknown)
	assert.Equal(s.T(), "unknown-service", tnUnknown.GetProps().DefaultMeta)
	assert.Equal(s.T(), "Figo", tnUnknown.GetString("field"))
	assert.Equal(s.T(), "Figo2", tnUnknown.GetString("field3"))
}

func (s *FsRepoNodeSuite) Test_path_meta_id() {
	path := "/metas/node_port/sub/name"
	val := strings.SplitN(path, "/", 4)

	assert.Equal(s.T(), 4, len(val))
	assert.Equal(s.T(), "node_port", val[2])
	assert.Equal(s.T(), "sub/name", val[3])
}

func (s *FsRepoNodeSuite) Test_patch_test() {
	patchJSON := []byte(`[
		{"op": "replace", "path": "/metas/meta1/name", "value": "Jane"},
		{"op": "remove", "path": "/height"},
		{"op": "add", "path": "/metas/meta_b/name", "value": "Mane"},
		{"op": "move", "path": "/metas/meta_b/mew", "value": "Alex"},
		{"op": "remove", "path": "/metas/meta_b", "value": "Alex"}
	]`)

	files := splitPatches(patchJSON)

	assert.Equal(s.T(), 3, len(files))
}

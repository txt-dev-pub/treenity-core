package repo

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	pjson "github.com/evanphx/json-patch/v5"
	"github.com/lithammer/shortuuid/v4"
	"github.com/spf13/afero"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	core "gitlab.com/txt-dev-pub/treenity-core"
	"os"
	osPath "path"
	"path/filepath"
	"strings"
)

type fsNodeRepository struct {
	fs       afero.Fs
	basePath string
}

func (r *fsNodeRepository) HasChildren(link core.Link) (bool, error) {
	ld, err := link.Parse()
	if err != nil {
		return false, err
	}

	nodePath := ld.NodesPath("")
	path := fmt.Sprintf("%s%s", r.basePath, nodePath)
	isDir, err := afero.IsDir(r.fs, path)
	if err != nil {
		return false, err
	}

	// Create tree node
	if isDir {
		filesList, err := afero.ReadDir(r.fs, path)
		if err != nil {
			return false, err
		}

		for _, info := range filesList {
			if info.IsDir() {
				return true, nil
			}
		}
		return false, nil
	}

	return false, errors.New("is-not-dir")
}

func (r *fsNodeRepository) fileContent(path string) ([]byte, error, *string) {
	var originPath *string
	file, err := r.fs.Open(path)

	if err != nil {
		path, err := os.Readlink(path)
		if err != nil {
			return nil, err, nil
		}

		file, err = r.fs.Open(path)
		if err != nil {
			panic(err)
		}

		newPath := osPath.Dir(path[len(r.basePath):])
		originPath = &newPath
	}

	defer file.Close()
	a, err := filepath.EvalSymlinks(path)
	if err == nil {
		file, err = r.fs.Open(a)
		if err != nil {
			return nil, err, nil
		}

		newPath := osPath.Dir(a[len(r.basePath):])
		originPath = &newPath
	}

	fileName := file.Name()
	data, err := afero.ReadFile(r.fs, fileName)
	if err != nil {
		return nil, err, originPath
	}

	return data, err, originPath
}

func (r *fsNodeRepository) readFile(path string) (*core.Meta, error, *string) {
	data, err, originPath := r.fileContent(path)

	var meta core.Meta
	err = json.Unmarshal(data, &meta)
	if len(meta.Id) == 0 {
		return nil, err, originPath
	}

	return &meta, err, originPath
}

func (r *fsNodeRepository) LinkTo(from core.Link, to core.Link) error {
	fromLd, err := from.Parse()
	if err != nil {
		return err
	}

	var path string
	var toPath string

	if fromLd.MetaExist() {
		path = fmt.Sprintf("%s%s.json", r.basePath, fromLd.NodeMetaPath())
	} else {
		path = fmt.Sprintf("%s%s", r.basePath, fromLd.NodeMetaPath())
	}

	exist, err := afero.Exists(r.fs, path)
	if !exist {
		return errors.New("link item not found")
	}

	if err != nil {
		return err
	}

	toLd, err := to.Parse()
	if err != nil {
		return err
	}

	if toLd.MetaExist() {
		toPath = fmt.Sprintf("%s%s.json", r.basePath, toLd.NodeMetaPath())
	} else {
		toPath = fmt.Sprintf("%s%s", r.basePath, toLd.NodeMetaPath())
	}

	existTo, err := afero.Exists(r.fs, toPath)
	if existTo {
		return errors.New("link already exist")
	}

	return os.Symlink(path, toPath)
}

func (r *fsNodeRepository) List(ctx context.Context, link core.Link, opts *sharedModels.ListOptions) ([]*core.TreeNode, error) {
	ld, err := link.Parse()
	if err != nil {
		return nil, err
	}

	metaPath := ld.NodesPath("")
	path := fmt.Sprintf("%s%s", r.basePath, metaPath)
	isDir, err := afero.IsDir(r.fs, path)
	if err != nil {
		return nil, err
	}

	// Create tree node
	if isDir {
		var items []*core.TreeNode

		filesList, err := afero.ReadDir(r.fs, path)
		if err != nil {
			return nil, err
		}

		// TODO: Add cursor moving
		for _, info := range filesList {
			if !info.IsDir() {
				continue
			}

			_path := filepath.Clean(fmt.Sprintf("%s/%s", metaPath, info.Name()))
			n, _ := r.getDir(_path, false)
			if n != nil {
				items = append(items, n)
			}
		}

		return items, nil
	}

	return nil, errors.New("is-not-dir")
}

func (r *fsNodeRepository) Copy(from core.Link, to core.Link) error {
	fromLd, err := from.Parse()
	if err != nil {
		return err
	}

	toLd, err := to.Parse()
	if err != nil {
		return err
	}

	nodePath := fromLd.NodesPath("")
	isMeta := fromLd.MetaExist()

	if toLd.IsSubPath(from) {
		return errors.New("copy.from.link.no-can-use")
	}

	fromDirExist, err := afero.DirExists(r.fs, r.getPath(nodePath))
	if err != nil {
		return err
	}

	if !fromDirExist {
		return errors.New("copy.from.dir-not-found")
	}

	toDirExist, err := afero.DirExists(r.fs, r.getPath(toLd.NodesPath("")))
	if err != nil {
		return err
	}

	node, err := r.getDir(nodePath, isMeta)
	if err != nil {
		return err
	}

	if !toDirExist {
		var lastNode *core.TreeNode
		for _, dir := range toLd.Nodes() {
			path := dir
			if lastNode != nil {
				path = fmt.Sprintf("%s/%s", lastNode.GetPath(), dir)
			} else {
				path = fmt.Sprintf("/%s", dir)
			}

			lid, err := core.Link(path).Parse()
			if err != nil {
				return err
			}

			empty := core.FromMetas(lid.LastPathItem(), lid.NodesPath(""))
			n := core.NewTreeNode(*empty, lastNode)
			_, err = r.Create(context.TODO(), &n)
			if err == nil {
				lastNode = empty
			}
		}
	}

	// TODO Check it
	//if isMeta {
	//	metaAny := r.Get(from)
	//	if metaAny == nil {
	//		return errors.New("not-found")
	//	}
	//
	//	meta := metaAny.(*core.Meta)
	//	node.AddMeta(meta)
	//}

	node.SetPath(toLd.NodeMetaPath())
	_, err = r.Create(context.TODO(), node)
	if err != nil {
		return err
	}

	return err
}

func (r *fsNodeRepository) setupNode(tn *core.TreeNode, meta *core.Meta, path string) {
	tn.Type = meta.Type
	tn.Id = meta.Id
	tn.Name = meta.Name
	tn.SetData(core.NodeProps{
		Metas: map[string]*core.Meta{},
	})
	tn.SetData(meta.Data)
	tn.Tags = meta.Tags

	tn.SetPath(path)
}

func (r *fsNodeRepository) getDir(metaPath string, onlyNode bool) (*core.TreeNode, error) {
	tn := core.NewTreeEmptyNode()

	path := r.basePath + metaPath
	filesList, err := afero.ReadDir(r.fs, path)
	if err != nil {
		return nil, err
	}

	for _, info := range filesList {
		if info.IsDir() || !strings.HasSuffix(info.Name(), ".json") {
			continue
		}

		fileName := info.Name()

		filePath := fmt.Sprintf("%s/%s", path, fileName)
		meta, err, _ := r.readFile(filePath)
		if err != nil || meta == nil || len(meta.Id) == 0 {
			fmt.Printf("🛑 Failed to read: %s. Error: %s", filePath, err.Error())
			continue
		}

		if fileName == "$.json" {
			r.setupNode(&tn, meta, metaPath)
			continue
		} else if onlyNode {
			continue
		}

		meta.Node = &tn

		tn.AddMeta(meta)
	}

	if len(tn.Id) == 0 {
		return nil, errors.New("node-parse-failed")
	}

	return &tn, err
}

func (r *fsNodeRepository) Get(link core.Link) *core.TreeNode {
	ld, err := link.Parse()
	if err != nil {
		return nil
	}

	path := fmt.Sprintf("%s%s", r.basePath, ld.NodesPath(""))

	isDir, err := afero.IsDir(r.fs, path)
	if err != nil {
		return nil
	}

	// Create tree node
	if isDir {
		tn := core.NewTreeEmptyNode()
		//TODO Test it

		filesList, err := afero.ReadDir(r.fs, path)
		if err != nil {
			return nil
		}

		for _, info := range filesList {
			if info.IsDir() {
				continue
			}

			fileName := info.Name()

			filePath := fmt.Sprintf("%s/%s", path, fileName)
			meta, err, originPath := r.readFile(filePath)

			if meta == nil {
				continue
			}

			if fileName == "$.json" {
				r.setupNode(&tn, meta, ld.NodesPath(""))
				continue
			}

			if originPath != nil {
				_tn, _ := r.getDir(*originPath, true)
				if _tn == nil {
					continue
				}

				meta.Node = _tn
			} else {
				meta.Node = &tn
			}

			if err != nil {
				continue
			}

			tn.AddMeta(meta)
		}

		if len(tn.Id) == 0 {
			return nil
		}

		return &tn
	}

	//tn, err := r.getDir(ld.NodesPath("), true)
	//
	//exist, err := afero.Exists(r.fs, path)
	//if err != nil {
	//	return nil
	//}
	//if !exist {
	//	return nil
	//}
	//
	//file, err := r.fs.Open(path)
	//if err != nil {
	//	return nil
	//}
	//
	//byt, err := afero.ReadFile(r.fs, file.Name())
	//if err != nil {
	//	return nil
	//}
	//var node core.TreeNode
	//err = json.Unmarshal(byt, &node)
	//if err != nil {
	//	return nil
	//}
	//
	//node.Node = tn

	return nil
}

func (r *fsNodeRepository) getPath(path string) string {
	return r.basePath + path
}

func (r *fsNodeRepository) Create(ctx context.Context, n *core.TreeNode) (*core.TreeNode, error) {
	if len(n.Name) == 0 {
		return nil, errors.New("no name")
	}

	n.Id = shortuuid.New()

	nPath := n.GetPath()
	if len(nPath) == 0 {
		return nil, errors.New("repo.create.path-is-empty")
	}

	path := r.getPath(n.GetPath())
	ok, err := afero.DirExists(r.fs, path)
	if err != nil {
		return nil, err
	}

	if !ok {
		err := r.fs.Mkdir(path, 0755)
		if err != nil {
			return nil, err
		}
	}

	props := core.GetData[core.NodeProps](n)
	if props != nil && props.Metas != nil {
		for _, m := range props.Metas {
			filePath := fmt.Sprintf("%s/%s.json", path, m.Name)
			file, err := r.fs.Create(filePath)
			if err != nil {
				return nil, err
			}

			bytes, err := json.Marshal(m)
			if err != nil {
				return nil, err
			}

			_, err = file.Write(bytes)
			if err != nil {
				return nil, err
			}

			file.Close()
		}
	}

	if n.Data == nil || n.Data.Get("metas") == nil {
		n.SetData(core.NodeProps{
			Metas: map[string]*core.Meta{},
		})
	}

	nCopy := *n
	//nCopy.Data = nil
	nCopy.Data = n.Data.Copy()
	delete(nCopy.Data, "metas")
	delete(nCopy.Data, "path")
	filePath := fmt.Sprintf("%s/$.json", path)
	file, err := r.fs.Create(filePath)
	if err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(&nCopy)
	if err != nil {
		return nil, err
	}

	_, err = file.Write(bytes)
	if err != nil {
		return nil, err
	}

	file.Close()

	return n, nil
}

// Tested
func splitPatches(patch []byte) map[string]string {
	files := make(map[string]string)

	items := gjson.ParseBytes(patch).Array()
	for _, p := range items {
		var docName string
		var value string
		path := p.Get("path").String()
		if strings.HasPrefix(path, "/metas") {
			val := strings.SplitN(path, "/", 4)
			docName = val[2]
			// TODO: Test
			//key := ""
			if len(val) > 3 {
				//key = val[3]
				value, _ = sjson.Set(p.Raw, "path", "/"+val[3])
			} else {
				value, _ = sjson.Set(p.Raw, "path", "")
				if p.Get("op").String() == "add" {
					value, _ = sjson.Set(value, "op", "replace")
				}
			}

		} else {
			docName = "$"
			value = p.Raw
		}

		f, ok := files[docName]
		if !ok {
			f = "[]"
		}

		files[docName], _ = sjson.SetRaw(f, "-1", value)
	}

	return files
}

func (r *fsNodeRepository) Patch(link core.Link, patches []byte) error {
	filePatches := splitPatches(patches)
	var data []byte

	ld, err := link.Parse()
	if err != nil {
		return err
	}

	for name, p := range filePatches {
		patch, err := pjson.DecodePatch([]byte(p))
		if err != nil {
			return err
		}

		path := fmt.Sprintf("%s%s%s.json", r.basePath, ld.NodesPath("/"), name)

		exist, err := afero.Exists(r.fs, path)
		if err != nil {
			return err
		}

		if !exist {
			// TODO DRY
			var m *core.Meta
			modified, err := patch.Apply([]byte("{}"))
			modified, err = sjson.SetBytes(modified, "$name", name)

			// XXX Temporary
			err = json.Unmarshal(modified, &m)
			if err != nil {
				return err
			}

			bytes, err := json.Marshal(m)
			if err != nil {
				return err
			}

			file, err := r.fs.Create(path)
			if err != nil {
				return err
			}
			_, err = file.Write(bytes)
			if err != nil {
				return err
			}

			return nil
		}

		isDir, err := afero.IsDir(r.fs, path)
		if err != nil {
			return err
		}

		if isDir {
			continue
		}

		// TODO: Bug maybe
		if len(patch) == 1 && patch[0].Kind() == "remove" {
			p, _ := patch[0].Path()
			if p == "" {
				err = r.fs.Remove(path)
				continue
			}
		}

		data, _, _ = r.fileContent(path)
		modified, err := patch.Apply(data)
		if err != nil {
			return err
		}

		file, err := r.fs.OpenFile(path, os.O_WRONLY|os.O_TRUNC, 0644)
		if err != nil {
			return err
		}

		_, err = file.Write(modified)
		file.Close()

		for _, v := range patch {
			_path, err := v.Path()
			if err != nil {
				continue
			}

			if _path == "/$name" {
				value, err := v.ValueInterface()
				if err != nil {
					fmt.Println(err)
					continue
				}

				newName := value.(string)

				if name == "$" {
					oldPath := fmt.Sprintf("%s%s", r.basePath, ld.NodesPath(""))
					newPath := fmt.Sprintf("%s%s/%s", r.basePath, ld.NodesPathWithTrim(1), newName)
					err = r.fs.Rename(oldPath, newPath)
				} else {
					newPath := fmt.Sprintf("%s%s%s.json", r.basePath, ld.NodesPath("/"), newName)
					err = r.fs.Rename(path, newPath)
				}

				if err != nil {
					fmt.Println(err)
					continue
				}

				break
			}
		}
	}

	return err
}

func (r *fsNodeRepository) CreateChild(ctx context.Context, parentId *string, n *core.TreeNode) (*core.TreeNode, error) {
	panic("implement me")
}

func (r *fsNodeRepository) FindById(ctx context.Context, id string) (*core.TreeNode, error) {
	//TODO implement me
	panic("implement me")
}

func (r *fsNodeRepository) FindBySlug(ctx context.Context, slug string) (*core.TreeNode, error) {
	//TODO implement me
	panic("implement me")
}

func (r *fsNodeRepository) FindByIdOrSlug(ctx context.Context, v string, pid any) (*core.TreeNode, error) {
	//TODO implement me
	panic("implement me")
}

func (r *fsNodeRepository) FindMeta(ctx context.Context, data map[string]any) ([]*core.Meta, error) {
	panic("implement me")
	var Type *string
	var Tag *string

	if v, ok := data["metas.$type"]; ok {
		value := v.(string)
		Type = &value
	}

	if v, ok := data["metas.$tg"]; ok {
		value := v.(string)
		Tag = &value
	}
	//TODO: path
	path := "private/services/tree-node/tree-service.tree-service"

	file, err := r.fs.Open(path)
	if err != nil {
		return nil, err
	}

	byt, err := afero.ReadFile(r.fs, file.Name())
	if err != nil {
		return nil, err
	}
	var node core.TreeNode
	err = json.Unmarshal(byt, &node)
	if err != nil {
		return nil, err
	}

	var metas []*core.Meta
	metas = append(metas, core.GetMetas(&node, Type, Tag)...)

	file.Close()
	return metas, nil
}

func (r *fsNodeRepository) FindMetaTT(ctx context.Context, tag string, typeValue string) ([]*core.Meta, error) {
	//TODO implement me
	panic("implement me")
}

func (r *fsNodeRepository) CreateOrGet(node *core.TreeNode) (*core.TreeNode, error) {
	//TODO implement me
	panic("implement me")
}

func NewFsNodeRepository(basePath string, handler afero.Fs) core.NodeRepo {
	ok, _ := afero.DirExists(handler, basePath)
	if !ok {
		err := handler.Mkdir(basePath, 0755)
		if err != nil {
			panic(err)
		}
	}

	return &fsNodeRepository{
		fs:       handler,
		basePath: basePath,
	}
}

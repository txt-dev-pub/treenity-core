package repo

import (
	"context"
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type IMetaProvider interface {
	CreateOrGet(link string, parent *core.TreeNode, metas ...*core.Meta) (*core.TreeNode, error)
}

type metaProvider struct {
	res  core.Loader
	repo core.NodeRepo
}

func (m *metaProvider) CreateOrGet(link string, parent *core.TreeNode, metas ...*core.Meta) (*core.TreeNode, error) {
	ld, err := core.Link(link).Parse()
	if err != nil {
		return nil, err
	}

	node := core.FromMetas(ld.LastPathItem(), ld.NodesPath(""), metas...)
	// TODO Fix root node path
	res := m.res.Get(node.GetLink())

	if res == nil || len(res.Name) == 0 {
		n := core.NewTreeNode(*node, parent)
		return m.repo.Create(context.Background(), &n)
	}

	return res, nil
}

func MetaProvider(r core.NodeRepo) IMetaProvider {
	return &metaProvider{
		res:  NewLoader(r),
		repo: r,
	}
}

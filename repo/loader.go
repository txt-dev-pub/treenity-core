package repo

import (
	core "gitlab.com/txt-dev-pub/treenity-core"
)

type structProvider struct {
	r core.NodeRepo
}

func (s *structProvider) Children(data core.Link) []*core.TreeNode {
	//TODO implement me
	panic("implement me")
}

type entity struct {
	meta  string
	lnk   core.Link
	value *core.TreeNode
}

func (e *entity) Meta() *core.Meta {
	if e.value == nil {
		return nil
	}

	p := e.value.GetProps()
	return p.Metas[e.meta]
}

func (e *entity) Node() *core.TreeNode {
	return e.value
}

type Entity interface {
	Meta() *core.Meta
	Node() *core.TreeNode
}

func (s *structProvider) Get(data core.Link) *core.TreeNode {
	return s.r.Get(data)
}

func NewLoader(r core.NodeRepo) core.Loader {
	return &structProvider{
		r: r,
	}
}

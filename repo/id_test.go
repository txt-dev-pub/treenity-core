package repo

import (
	"fmt"
	"github.com/lithammer/shortuuid/v4"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_generateId(t *testing.T) {
	for i := 0; i < 100; i++ {
		fmt.Println(shortuuid.New())
	}

	assert.Equal(t, "sub/name", "sub/name")
}

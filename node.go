package treenityCore

import (
	"golang.org/x/exp/slices"
)

type NodeProps struct {
	DefaultMeta string           `json:"defaultMeta,omitempty"`
	Metas       map[string]*Meta `json:"metas"`
}

func (p *NodeProps) Add(meta *Meta) {
	if p.Metas == nil {
		p.Metas = make(map[string]*Meta)
	}
	p.Metas[meta.Name] = meta
}

type Node = Meta

func (n *Node) GetProps() *NodeProps {
	return GetData[NodeProps](n)
}

// TODO No parse
func (n *Node) AddMeta(meta *Meta) {
	props := n.GetProps()
	props.Add(meta)
	n.SetData(props)
}

func (n *Node) MetaByName(name string) *Meta {
	props := n.GetProps()
	if props == nil {
		return n
	}

	m, ok := props.Metas[name]
	if ok {
		return m
	}

	return n
}

func GetMetas(n *Node, typeValue *string, tag *string) []*Meta {
	var metas []*Meta
	props := n.GetProps()

	for _, m := range props.Metas {
		if typeValue != nil && tag != nil {
			if slices.Contains(m.Tags, *tag) && m.Type == *typeValue {
				m.Node = n
				metas = append(metas, m)
			}

			continue
		}

		if typeValue != nil {
			if m.Type == *typeValue {
				m.Node = n
				metas = append(metas, m)
			}

			continue
		}

		if tag != nil {
			if slices.Contains(m.Tags, *tag) {
				m.Node = n
				metas = append(metas, m)
			}
		}
	}

	return metas
}

func NewNode(metas ...*Meta) Node {
	items := make(map[string]*Meta)
	for _, m := range metas {
		items[m.Name] = m
	}

	return Node{
		Type: "node",
		Data: Data{
			"metas": items,
		},
	}
}

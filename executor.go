package treenityCore

import (
	"context"
	"fmt"
)

type LinkExecutor interface {
	ExecuteLink(lnk Link, params Data, pipe Pipe)
	ExecuteLinkRes(lnk Link, params Data) Data
}

type Executor interface {
	LinkExecutor
	Exec(ctx string, typeValue string, params Data, pipe Pipe)
}

type executor struct {
	reg    Registry
	loader Loader
}

func (e *executor) ExecuteLinkRes(lnk Link, params Data) Data {
	ch := make(chan Data)
	go e.ExecuteLink(lnk, params, ch)
	return <-ch
}

func (e *executor) ExecuteLink(lnk Link, params Data, pipe Pipe) {
	item := e.loader.Get(lnk)
	if item == nil {
		pipe <- NodeDataErr("not-found")
		return
	}

	if params == nil {
		params = Data{}
	}

	params.Set(ItemKey, item)
	params.Set(MethodKey, lnk.Field())
	e.Exec(lnk.Context(), item.Type, params, pipe)
}

func (e *executor) Exec(ctx string, typeValue string, data Data, pipe Pipe) {
	runnable, err := e.reg.Get(RegParams{
		Context: ctx,
		Type:    typeValue,
	})

	if err != nil {
		fmt.Println("🛑 Not found runnable")
		pipe <- NodeDataError(err)
		return
	}

	runnable(context.TODO(), data, pipe)
}

func NewExecutor(reg Registry, loader Loader) Executor {
	return &executor{
		reg:    reg,
		loader: loader,
	}
}

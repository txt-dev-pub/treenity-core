package treenityCore

import (
	"reflect"
	"strings"
)

func StructToMap(item interface{}) map[string]interface{} {
	res := map[string]interface{}{}
	if item == nil {
		return res
	}
	v := reflect.TypeOf(item)
	reflectValue := reflect.ValueOf(item)
	reflectValue = reflect.Indirect(reflectValue)

	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	for i := 0; i < v.NumField(); i++ {
		tag := v.Field(i).Tag.Get("json")
		field := reflectValue.Field(i).Interface()
		if tag != "" && tag != "-" {
			tagKey := strings.Split(tag, ",")[0]

			if v.Field(i).Type.Kind() == reflect.Struct {
				res[tagKey] = StructToMap(field)
			} else {
				res[tagKey] = field
			}
		}
	}
	return res
}

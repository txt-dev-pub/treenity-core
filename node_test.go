package treenityCore

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNodeLink(t *testing.T) {
	m := &TreeNode{
		Id:   "my-id",
		Name: "general",
		Type: "node-tree",
		Data: Data{
			"title": "FolderName",
			"links": []any{
				map[string]any{
					"p": "",
					"v": "txt://private/modules",
				},
				map[string]any{
					"p": "child",
					"v": "relative",
				},
				map[string]any{
					"p": "child#children",
					"v": "absolute",
				},
				map[string]any{
					"p": "child-with-method#method",
					"v": "with-method",
				},
			},
			"path":        "/str",
			"defaultMeta": "example",
			"metas": map[string]*Meta{
				"child": {
					Id:   "child-id",
					Name: "child",
					Type: "child-type",
				},
				"child-with-method": {
					Id:   "child-with-method-id",
					Name: "child-with-method",
					Type: "child-with-method-type",
				},
				"relative": {
					Id:   "relative-id",
					Name: "relative",
					Type: "relative-type",
				},
				"with-method": {
					Id:   "with-method-id",
					Name: "with-method",
					Type: "with-method-type",
				},
			},
		},
	}

	// select only meta with unknown method
	lnk1 := Link("txt://tree.repo/private/general/$child#flip")
	ld1, err := lnk1.Parse()
	assert.NoError(t, err)

	link1 := m.MetaLink(ld1)
	assert.Equal(t, "relative", link1)

	// same meta with children method
	lnk2 := Link("txt://tree.repo/private/general/$child#children")
	ld2, err := lnk2.Parse()
	assert.NoError(t, err)

	link2 := m.MetaLink(ld2)
	assert.Equal(t, "absolute", link2)

	// exactly method as in link
	lnk3 := Link("txt://tree.repo/private/general/$child-with-method#method")
	ld3, err := lnk3.Parse()
	assert.NoError(t, err)

	link3 := m.MetaLink(ld3)
	assert.Equal(t, "with-method", link3)
	assert.Equal(t, "absolute", link2)

	// no linked meta
	lnk4 := Link("txt://tree.repo/private/general/$any-meta")
	ld4, err := lnk4.Parse()
	assert.NoError(t, err)

	link4 := m.MetaLink(ld4)
	assert.Equal(t, "any-meta", link4)

	// link on node
	lnk5 := Link("txt://tree.repo/private/general")
	ld5, err := lnk5.Parse()
	assert.NoError(t, err)

	link5 := m.MetaLink(ld5)
	assert.Equal(t, "txt://private/modules", link5)
}

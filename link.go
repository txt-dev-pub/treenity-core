package treenityCore

import (
	"fmt"
	"net/url"
	"regexp"
	"strings"
)

const TreeSchema = "tree"

type LinkData struct {
	path      []string
	Schema    string `json:"schema"`
	pathExist bool
	hash      string
	Query     map[string][]string `json:"query"`
}

type Link string

func CreateStructLinkN(path string) Link {
	return CreateLnk(TreeSchema, path, "", nil)
}

func CreateStructLinkNF(path string, field string) Link {
	return CreateLnk(TreeSchema, path, "", &field)
}

func CreateStructLinkNC(context string, path string) Link {
	return CreateLnk(context, path, "", nil)
}

func CreateStructLinkNFC(context string, path string, field string) Link {
	return CreateLnk(context, path, "", &field)
}

func CreateStructLinkNM(path string, meta string) Link {
	return CreateLnk(TreeSchema, path, meta, nil)
}

func CreateStructLinkNMC(context string, path string, meta string) Link {
	return CreateLnk(context, path, meta, nil)
}

func CreateStructLinkNMF(path string, meta string, field string) Link {
	return CreateLnk(TreeSchema, path, meta, &field)
}

func CreateLnk(schema string, path string, meta string, field *string) Link {
	urlItem := fmt.Sprintf("%s:%s", schema, path)

	if len(meta) > 0 {
		urlItem = fmt.Sprintf("%s/$%s", urlItem, meta)
	}

	if field != nil && len(*field) > 0 {
		urlItem = fmt.Sprintf("%s#%s", urlItem, *field)
	}

	return Link(urlItem)
}

func (l Link) Schema() string {
	if strings.ContainsAny(string(l), ":") {
		return strings.Split(string(l), ":")[0]
	}

	return TreeSchema
}

func (l Link) Join(elem ...string) Link {
	var data []string

	if strings.ContainsAny(string(l), "$") {
		data = append(data, "../")
		data = append(data, elem...)
	} else {
		data = elem
	}

	s, err := url.JoinPath(string(l), data...)
	if err != nil {
		return ""
	}

	return Link(s)
}

func (ld *LinkData) LastPathItem() string {
	return ld.path[len(ld.path)-1]
}

func (l Link) Parse() (*LinkData, error) {
	u, err := url.Parse(string(l))
	if err != nil {
		return nil, err
	}

	var query map[string][]string
	if u.ForceQuery {
		query = map[string][]string{}
		for key, value := range u.Query() {
			query[key] = value
		}
	}

	var path []string
	if len(u.Path) > 0 {
		path = strings.Split(u.Path[1:], "/")
	} else {
		path = []string{}
	}

	return &LinkData{
		Schema:    u.Scheme,
		pathExist: len(u.Path) > 0,
		path:      path,
		hash:      u.Fragment,
		Query:     query,
	}, nil
}

var contextRe = regexp.MustCompile(`^([a-z-0-9\-_+]+)`)
var fieldRe = regexp.MustCompile(`#([a-zA-Z0-9\-_]+)`)
var metaRe = regexp.MustCompile(`/\$([a-zA-Z0-9\-_]+)`)
var protocolAndHostRe = regexp.MustCompile(`^([a-z][a-z0-9+\-.]+):\/\/([a-z0-9\-._~%!$&'()*+,;=]+)`)

func (l Link) Context() string {
	return contextRe.FindString(string(l))
}

func (l Link) SetContext(c string) Link {
	oldContext := contextRe.FindString(string(l))

	if len(oldContext) == 0 {
		c = c + ":"
	}

	return Link(strings.Replace(string(l), oldContext, c, 1))
}

func (l Link) AddField(field string) Link {
	sl := string(l)
	i := strings.IndexAny(sl, "#")

	if i >= 0 {
		if len(field) > 0 {
			return Link(fmt.Sprintf("%s#%s", l[:i], field))
		}

		return Link(fmt.Sprintf("%s", l[:i]))
	}

	return Link(fmt.Sprintf("%s#%s", l, field))
}

func (l Link) Field() string {
	res := fieldRe.FindString(string(l))
	if len(res) > 1 {
		return res[1:]
	}

	return ""
}

func (l Link) Meta() string {
	res := metaRe.FindString(string(l))
	if len(res) > 2 {
		return res[2:]
	}

	return ""
}

func (l Link) ContextAndHost() string {
	res := protocolAndHostRe.FindString(string(l))
	if len(res) > 0 {
		return res
	}

	return ""
}

func (l Link) Str() string {
	return string(l)
}

func (l Link) SetHost(baseUrl string) Link {
	host := l.ContextAndHost()
	if len(host) != 0 {
		if host == baseUrl {
			return l
		}

		sLink := string(l)
		return Link(baseUrl + sLink[len(host):])
	}

	sLink := string(l)
	ctx := l.Context()
	if len(ctx) > 0 {
		return Link(baseUrl + sLink[len(ctx)+1:])
	}

	return Link(baseUrl + sLink)
}

package treenityCore

const ItemKey = "__item"
const MethodKey = "__method"
const TypeKey = "__type"
const LnkKey = "__lnk"

const ExecuteContext = "execute"
const ServerContext = "server"
const ServiceContext = "srv"
const GoServiceContext = "go-srv"
const KuberContext = "k8s"
const TreeContext = "tree"
const TxtContext = "txt"

package treenityCore

import (
	"github.com/lithammer/shortuuid/v4"
	"strings"
)

type NodeLink struct {
	Path  string `json:"p" mapstructure:"p"`
	Value string `json:"v" mapstructure:"v"`
}

type TreeProps struct {
	Title string     `json:"title"`
	Links []NodeLink `json:"links"`
	Path  string     `json:"path"`
}

type TreeNode = Node

// MetaLink TODO Optimize
func (tn *TreeNode) MetaLink(ld *LinkData) string {
	var v string
	field := ld.Field()
	meta := ""
	if ld.MetaExist() {
		meta = *ld.Meta()
		v += meta
	}

	if field != nil {
		v += "#" + *field
	}

	for _, l := range tn.GetLinks() {
		if l.Path == v {
			return l.Value
		}
	}

	for _, l := range tn.GetLinks() {
		if l.Path == meta {
			return l.Value
		}
	}

	return meta
}

func (tn *TreeNode) GetLinks() []NodeLink {
	p := GetData[TreeProps](tn)
	return p.Links
}

func (tn *TreeNode) SetTitle(title string) {
	tn.Data.Set("title", title)
}

func (tn *TreeNode) SetPath(path string) {
	if tn.Data == nil {
		tn.Data = Data{}
	}

	if strings.HasPrefix(path, "//") {
		path = strings.ReplaceAll(path, "//", "/")
	}

	tn.Data.Set("path", path)
}

func (tn *TreeNode) SetHasChildren(hasChildren bool) {
	if tn.Data == nil {
		tn.Data = Data{}
	}

	tn.Data.Set("hasChildren", hasChildren)
}

func (tn *TreeNode) GetPath() string {
	if tn.Type != "tree-node" && tn.Node != nil {
		return tn.Node.(*TreeNode).GetPath()
	}
	return tn.Data.GetString("path")
}

func (tn *TreeNode) GetMetaByType(mType string) *Meta {
	props := tn.GetProps()
	for _, m := range props.Metas {
		if m.Type == mType {
			return m
		}
	}

	return nil
}

func (tn *TreeNode) GetLink() Link {
	return CreateStructLinkN(tn.GetPath())
}

func FromMetas(name string, path string, metas ...*Meta) *TreeNode {
	node := NewNode(metas...)
	if len(name) == 0 {
		node.Name = "root"
	} else {
		node.Name = name
	}

	node.SetPath(path)
	return &node
}

func NewTreeEmptyNode() TreeNode {
	return TreeNode{
		Type: "node_tree",
		Data: StructToMap(NodeProps{
			Metas: map[string]*Meta{},
		}),
	}
}

func NewTreeNode(from TreeNode, parent *TreeNode) TreeNode {
	m := NewTreeEmptyNode()

	m.Id = shortuuid.New()
	m.Name = from.Name
	m.Tags = from.Tags
	title := ""
	if v, ok := from.Data["title"]; ok {
		title = v.(string)
	}

	if parent == nil {
		m.SetData(
			TreeProps{
				Title: title,
				Path:  "/",
			},
		)
	} else {
		m.SetData(
			TreeProps{
				Path:  parent.GetPath(),
				Title: title,
			},
		)
	}

	m.Data.MergeHere(from.Data)

	return m
}

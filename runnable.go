package treenityCore

import (
	"context"
	"time"
)

type M map[string]Runnable
type Pipe chan<- Data
type Runnable = func(ctx context.Context, in Data, out Pipe)

func Once(in Data, r Runnable) Data {
	ctx, cancel := context.WithCancel(context.TODO())
	timeCtx, _ := context.WithTimeout(ctx, 1*time.Second)
	ch := make(chan Data)
	go r(timeCtx, in, ch)
	res := <-ch
	cancel()
	close(ch)
	return res
}

func CreateService(methods M, loop Runnable) Runnable {
	data := Data{}
	data.Set("methods", methods)

	return func(ctx context.Context, in Data, out Pipe) {
		out <- data
		if loop != nil {
			go loop(ctx, in, out)
		}
	}
}
